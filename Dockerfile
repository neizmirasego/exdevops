FROM python:3.8.3-buster

RUN mkdir -p /exdevops && \
    groupadd -r devops && \
    useradd -r -g devops devops

WORKDIR /exdevops

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
  apt-get install -y postgresql postgresql-contrib && \
  rm -rf /var/lib/apt/lists/*

COPY requirements requirements
RUN pip install --no-cache-dir -r requirements/main.txt

COPY ./manage.py .
COPY ./README.md .
COPY ./configs configs
COPY ./src src

EXPOSE 8000
USER devops
CMD ["python", "./manage.py", "runserver"]
